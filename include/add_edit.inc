<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

/**
 *
 * function used by the add question scripts
 *
 * @author Simon Wilkinson, Anthony Brown
 * @version 1.0
 * @copyright Copyright (c) 2014 The University of Nottingham
 * @package
 */

require_once 'errors.php';

/**
 * Takes in an HTML string and trys to remove as much superfluous formatting as possible.
 *
 * @param mixed $sHTML
 *
 */
function clearMSOtags($sHTML)
{

    if (trim($sHTML) == '<p>&nbsp;</p>' or trim($sHTML) == '<p></p>' or trim($sHTML) == '<div>&nbsp;</div>' or trim($sHTML) == '<div></div>' or trim($sHTML) == '<br />') {
        $sHTML = '';
    } else {
        if (mb_strpos($sHTML, 'mso-') !== false or mb_strpos($sHTML, 'MsoNormal') !== false or mb_strpos($sHTML, '<o:p>') !== false) {
            $allowedTags = '<div><b><i><strong><em><sup><sub><table><tr><td><tbody><th>';
            $sHTML = strip_tags($sHTML, $allowedTags);
        }
        // Further processing.
        $sHTML = str_ireplace('PADDING-TOP: 0cm', '', $sHTML);
        $sHTML = str_ireplace('PADDING-BOTTOM: 0cm', '', $sHTML);
        $sHTML = str_ireplace('BACKGROUND-COLOR: transparent;', '', $sHTML);
        $sHTML = str_ireplace('<o:p></o:p>', '', $sHTML);
        $sHTML = str_ireplace('<strong></strong>', '', $sHTML);
        $sHTML = str_ireplace('COLOR: black', '', $sHTML);

        // Convert strange codes.
        $sHTML = str_ireplace('%u2013', '-', $sHTML);
        $sHTML = str_ireplace('%u2018', "'", $sHTML);
        $sHTML = str_ireplace('%u2019', "'", $sHTML);

        // Some regular expressions
        $sHTML = preg_replace('/FONT-SIZE: [0-9]+pt/', '', $sHTML);
        $sHTML = preg_replace('/FONT-FAMILY: [\'a-zA-Z 0-9,]*/', '', $sHTML);
        $sHTML = preg_replace('/FONT: [0-9]+pt/', '', $sHTML);
        $sHTML = preg_replace('/LINE-HEIGHT: [0-9]+%/', '', $sHTML);

        $sHTML = preg_replace('/lang="[\-A-Za-z]*"/', '', $sHTML);
        $sHTML = preg_replace('/mso-[\-A-Za-z]+: [_\. \'#\-A-Za-z0-9,]+/', '', $sHTML);

        // Do at end.
        $sHTML = str_ireplace('  ', ' ', $sHTML);
        $sHTML = str_ireplace(' ;', '', $sHTML);
        $sHTML = str_ireplace('style="; ', 'style="', $sHTML);
        $sHTML = str_ireplace('style=""', '', $sHTML);
        $sHTML = str_ireplace('style=" "', '', $sHTML);
        $sHTML = str_ireplace('<span >', '<span>', $sHTML);
        $sHTML = str_ireplace('; ">', '">', $sHTML);
    }
    return $sHTML;
}

/**
 * Create wysiwyg editor.
 *
 * @param string $name - name for editor
 * @param string $replace - id for editor
 * @param string $content - current content
 * @param string $type - TinyMCE only - type of editor i.e. standard
 * @return html of editor
 */
function wysiwyg_editor($name, $replace, $content = '', $type = '')
{
    global $string;
    $html = '';
    $content = htmlspecialchars($content);
    $texteditorplugin = \plugins\plugins_texteditor::get_editor();
    $html .=  $texteditorplugin->get_textarea($name, $replace, $content, $type);
    return $html;
}

function option_order($selected = '')
{
    $items = array('Display Order','Alphabetic','Random');

    $html = "<select id=\"option_order\" name=\"option_order\">\n";
    foreach ($items as $item) {
        if ($selected == mb_strtolower($item)) {
            $html .= '<option value="' . mb_strtolower($item) . "\" selected>$item</option>\n";
        } else {
            $html .= '<option value="' . mb_strtolower($item) . "\">$item</option>\n";
        }
    }
    $html .= "</select>\n";

    if ($selected != '') {
        $html .= '<input type="hidden" name="old_option_order" value="' . $selected . '" />';
    }

    return $html;
}

function render_changes($changes, &$string)
{
    $cutoff = 70;
    $i = 0;
    if (count($changes) > 0) {
        $html = <<< HTML
        <table class="data">
          <thead>
            <tr>
              <th class="data-small">{$string['date']}</th>
              <th class="data-small">{$string['action']}</th>
              <th>{$string['section']}</th>
              <th class="data-small">{$string['old']}</th>
              <th class="data-small">{$string['new']}</th>
              <th class="data-small">{$string['editor']}</th>
            </tr>
          </thead>
          <tbody>

HTML;

        foreach ($changes as $change) {
            $alt = ($i++ % 2 == 1) ? ' class="alt"' : '';
            $change['old'] = strip_tags($change['old']);
            $change['new'] = strip_tags($change['new']);
            $change_old = $change['old'];
            $change_old_more = '';
            $change_new = $change['new'];
            $change_new_more = '';
            if (mb_strlen($change['old']) > $cutoff) {
                $split_pos = mb_strrpos(mb_substr($change['old'], 0, $cutoff), ' ');
                $change_old = mb_substr($change['old'], 0, $split_pos);
                $change_old_more = mb_substr($change['old'], $split_pos);
                $change_old_more = "<span class=\"more-ellip\">&hellip;</span><span class=\"more-text\">{$change_old_more}</span><br /><a href=\"#\" class=\"more\">{$string['showmore']}</a>";
            }
            if (mb_strlen($change['new']) > $cutoff) {
                $split_pos = mb_strrpos(mb_substr($change['new'], 0, $cutoff), ' ');
                $change_new = mb_substr($change['new'], 0, $split_pos);
                $change_new_more = mb_substr($change['new'], $split_pos);
                $change_new_more = "<span class=\"more-ellip\">&hellip;</span><span class=\"more-text\">{$change_new_more}</span><br /><a href=\"#\" class=\"more\">{$string['showmore']}</a>";
            }
            if ($change['section'] == $string['markscorrect'] or $change['section'] == $string['marksincorrect'] or $change['section'] == $string['markspartial']) {
                $change['section'] = '<span style="color:#C00000">' . $change['section'] . '</span>';
            }
            $html .= <<< HTML
            <tr{$alt}>
              <td>{$change['date']}</td>
              <td>{$change['action']}</td>
              <td>{$change['section']}</td>
              <td>{$change_old}{$change_old_more}</td>
              <td>{$change_new}{$change_new_more}</td>
              <td>{$change['user']}</td>
            </tr>

HTML;
        }

        $html .= <<< HTML

            </tbody>
        </table>

HTML;
    } else {
        // Shouldn't happen if we're recording question creation as a change
        $html = <<< HTML
        <p>{$string['nochangesrecorded']}</p>

HTML;
    }

    return $html;
}

function render_full_history($changes, &$string, $qID)
{
    $cutoff = 70;
    $i = 0;
    if (count($changes) > 0) {
        $html = <<< HTML
        <table class="data full-history">
          <thead>
            <tr>
              <th class="data-smaller">{$string['date']}</th>
              <th class="data-qid">{$string['question']}</th>
              <th class="data-small">{$string['action']}</th>
              <th class="data-small">{$string['section']}</th>
              <th class="data-wide">{$string['old']}</th>
              <th class="data-wide">{$string['new']}</th>
              <th class="data-small">{$string['editor']}</th>
            </tr>
          </thead>
          <tbody>

HTML;

        foreach ($changes as $change) {
            $alt = ($i++ % 2 == 1) ? ' class="alt"' : '';
            $change['old'] = strip_tags($change['old']);
            $change['new'] = strip_tags($change['new']);
            $change_old = $change['old'];
            $change_old_more = '';
            $change_new = $change['new'];
            $change_new_more = '';
            if (mb_strlen($change['old']) > $cutoff) {
                $split_pos = mb_strrpos(mb_substr($change['old'], 0, $cutoff), ' ');
                $change_old = mb_substr($change['old'], 0, $split_pos);
                $change_old_more = mb_substr($change['old'], $split_pos);
                $change_old_more = "<span class=\"more-ellip\">&hellip;</span><span class=\"more-text\">{$change_old_more}</span><br /><a href=\"#\" class=\"more\">{$string['showmore']}</a>";
            }
            if (mb_strlen($change['new']) > $cutoff) {
                $split_pos = mb_strrpos(mb_substr($change['new'], 0, $cutoff), ' ');
                $change_new = mb_substr($change['new'], 0, $split_pos);
                $change_new_more = mb_substr($change['new'], $split_pos);
                $change_new_more = "<span class=\"more-ellip\">&hellip;</span><span class=\"more-text\">{$change_new_more}</span><br /><a href=\"#\" class=\"more\">{$string['showmore']}</a>";
            }
            if ($change['section'] == $string['markscorrect'] or $change['section'] == $string['marksincorrect'] or $change['section'] == $string['markspartial']) {
                $change['section'] = '<span class="marks-change">' . $change['section'] . '</span>';
            }
            if ($change['qID'] == $qID) {
                $change['qID'] = '<span class="current-question">' . $qID . '</span>';
            } else {
                $change['qID'] = '<a href="/question/edit/index.php?q_id=' . $change['qID'] . '&amp;tab=changes">' . $change['qID'] . '</a>';
            }
            $html .= <<< HTML
            <tr{$alt}>
              <td>{$change['date']}</td>
              <td>{$change['qID']}</td>
              <td>{$change['action']}</td>
              <td>{$change['section']}</td>
              <td>{$change_old}{$change_old_more}</td>
              <td>{$change_new}{$change_new_more}</td>
              <td>{$change['user']}</td>
            </tr>

HTML;
        }

        $html .= <<< HTML

            </tbody>
        </table>

HTML;
    } else {
        // Shouldn't happen if we're recording question creation as a change
        $html = <<< HTML
        <p>{$string['nochangesrecorded']}</p>

HTML;
    }

    return $html;
}

function render_comments($comments, &$string)
{
    if (count($comments) > 0) {
        $html = <<< HTML
        <table class="data" summary="{$string['reviewerscomments']}">
          <thead>
            <tr>
              <th class="data-tiny"></th>
              <th class="data-medium">{$string['reviewer']}</th>
              <th>{$string['comments']}</th>
              <th class="data-small">{$string['actiontaken']}</th>
              <th>{$string['internalresponse']}</th>
            </tr>
        </thead>
        <tbody>

HTML;

        $i = 0;
        foreach ($comments as $id => $comment) {
            switch ($comment['category']) {
                case 1:
                    $status = array('ok', $string['ok']);
                    break;
                case 2:
                    $status = array('minor', $string['minor']);
                    break;
                case 3:
                    $status = array('major', $string['major']);
                    break;
                default:
                    $status = array('cannot', $string['cannot']);
                    break;
            }
            $img_status = $status[0];

            $alt = ($i % 2 == 0) ? ' class="alt"' : '';
            $comment_str = ($comment['comment'] == '') ? $string['nocomments'] : nl2br($comment['comment']);
            $type_translated = $string[mb_strtolower($comment['type'])];

            $html .= <<< HTML
  					<tr{$alt}>
              <td><img src="../../artwork/{$img_status}_comment.png" width="16" height="16" alt="{$status[1]}" /></td>
              <td>{$comment['name']}<br /><span class="note">{$type_translated}</span></td>
              <td>{$comment_str}</td>
              <td>
                <select name="actions[]">

HTML;

            $actions = array('Not actioned' => $string['notactioned'], 'Read - disagree' => $string['readdisagree'], 'Read - actioned' => $string['readactioned']);

            foreach ($actions as $action => $act_text) {
                $sel = ($comment['action'] == $action) ? ' selected="selected"' : '';
                $html .= <<< HTML
      						<option value="{$action}"{$sel}>{$act_text}</option>

HTML;
            }

            $html .= <<< HTML
                  </select>
                <input type="hidden" name="comment_ids[]" value="{$id}" />
              </td>
              <td>
                <textarea cols="60" rows="3" name="responses[]">{$comment['response']}</textarea>
              </td>
            </tr>

HTML;
            $i++;
        }

        $html .= <<< HTML
            </tbody>
        </table>

HTML;
    } else {
        $html = <<< HTML
        <p>{$string['commentsmsg']}</p>

HTML;
    }

    return $html;
}
