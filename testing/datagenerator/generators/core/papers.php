<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

namespace testing\datagenerator;

use Config;
use yearutils;
use assessment;
use UserUtils;

/**
 * Generates Rogo paper.
 *
 * @author Yijun Xue <yijun.xue@nottingham.ac.uk>
 * @copyright Copyright (c) 2016 The University of Nottingham
 * @package testing
 * @subpackage datagenerator
 */
class papers extends generator
{
    /**
     * @var array default paper properties
     */
    private $default_paper_properties = array(
        'paper_prologue' => null, 'paper_postscript' => null, 'bgcolor' => 'white',
        'fgcolor' => 'black', 'themecolor' => '#316AC5', 'labelcolor' => '#C00000',
        'fullscreen' => '1', 'marking' => '1', 'bidirectional' => '1',
        'pass_mark' => 40, 'distinction_mark' => 70, 'folder' => null,
        'rubric' => null, 'calculator' => 1, 'random_mark' => 0.0, 'total_mark' => 0,
        'display_correct_answer' => '1', 'display_question_mark' => '1', 'display_students_response' => '1',
        'display_feedback' => '1', 'hide_if_unanswered' => '0', 'external_review_deadline' => null,
        'internal_review_deadline' => null, 'sound_demo' => '0', 'password' => null
    );

    /**
     * Creates security metadata restrictions for a paper.
     *
     * Parameters:
     * - name string The type of metadata
     * - value string The value of the metadata a student must have
     *
     * @param int $paperid The database id of the paper.
     * @param array|stdClass $parameters
     * @return array
     * @throws data_error
     */
    public function createSecurityMetadata(int $paperid, $parameters): array
    {
        // If an object is passed convert it into an array.
        if (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        // Check that the right type has been passed.
        if (!is_array($parameters)) {
            throw new data_error('Must pass an array or object');
        }
        $defaults = array(
            'paperID' => $paperid,
            'name' => 'SomeType',
            'value' => 'Default',
        );
        $values = $this->set_defaults_and_clean($defaults, $parameters);
        $values['id'] = $this->insertSecurityMetadata($values);
        return $values;
    }

    /**
     * Creates (non security) metadata restrictions for a paper.
     *
     * Parameters:
     * - name string The type of metadata
     * - value string The value of the metadata a student must have
     *
     * @param int $paperid The database id of the paper.
     * @param array|stdClass $parameters
     * @return array
     * @throws data_error
     */
    public function createMetadata(int $paperid, $parameters): array
    {
        // If an object is passed convert it into an array.
        if (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        // Check that the right type has been passed.
        if (!is_array($parameters)) {
            throw new data_error('Must pass an array or object');
        }
        $defaults = array(
            'paperID' => $paperid,
            'name' => 'SomeType',
            'value' => 'Default',
        );
        $values = $this->set_defaults_and_clean($defaults, $parameters);
        $values['id'] = $this->insertMetadata($values);
        return $values;
    }

    /**
     * Create a new paper with 4 mandatory or optional parametera
     *
     * @param array $parameters
     * Mandatories param in $parameters are
     *  string parameters[papertitle]
     *  string parameters[papertype], any of
     *    assessment::TYPE_PROGRESS,
     *    assessment::TYPE_SUMMATIVE,
     *    assessment::TYPE_SURVEY,
     *    assessment::TYPE_OSCE,
     *    assessment::TYPE_OFFLINE,
     *    assessment::TYPE_PEERREVIEW
     *  string parameters[paperowner]
     *  string parameters[modulename]
     * @throws data_error If passed parameter is invalid
     * @return array
     */
    public function create_paper($parameters)
    {

        if (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        // Check that the right type has been passed.
        if (!is_array($parameters)) {
            throw new data_error('Must pass an array or object');
        }
        if (empty($parameters['papertitle']) or !is_numeric($parameters['papertype']) or empty($parameters['paperowner']) or empty($parameters['modulename'])) {
            throw new data_error('Error in | papertitle | papertype | paperowner | modulename |');
        } else {
            $papertitle = $parameters['papertitle'];
            $papertype = (int)$parameters['papertype'];
            $paperowner = UserUtils::username_exists($parameters['paperowner'], $this->db);
            $modulename = $parameters['modulename'];
        }
        $default = array(
            'startdate' => null,
            'enddate' => null,
            'labs' => null,
            'duration' => null,
            'session' => null,
            'timezone' => 'Europe/London',
            'externalid' => null,
            'externalsys' => null,
            'calendaryear' => null,
            'remote' => 0,
            'settings' => '',
            'deleted' => null
        );
        $settings = $this->set_defaults_and_clean($default, $parameters);
        $timezone = new \DateTimeZone($settings['timezone']);

        if (!empty($settings['startdate'])) {
            try {
                $settings['start_date'] = \date_utils::getUTCDateTime($settings['startdate'], $settings['timezone'])->getTimestamp() ;
            } catch (\Exception $e) {
                // If user's date is not valid
                throw new data_error("Paper's startdate is wrong");
            }
        } else {
            // We need to be specific with start and end date as some paper types cannot run over into another day.
            $startdate = new \DateTime('tomorrow', $timezone);
            $startdate->setTime(12, 00);
            $settings['startdate'] = $startdate->format('Y-m-d H:i:s');
            $settings['start_date'] = $startdate->getTimestamp();
        }
        if (!empty($settings['enddate'])) {
            try {
                $settings['end_date'] = \date_utils::getUTCDateTime($settings['enddate'], $settings['timezone'])->getTimestamp() ;
            } catch (\Exception $e) {
                // If user's date is not valid
                throw new data_error("Paper's enddate is wrong");
            }
        } else {
            $enddate = new \DateTime('tomorrow', $timezone);
            $enddate->setTime(13, 00);
            $settings['enddate'] = $enddate->format('Y-m-d H:i:s');
            $settings['end_date'] = $enddate->getTimestamp();
        }

        $conf = Config::get_instance();

        if (!is_null($settings['deleted'])) {
            $deleted = \date_utils::getUTCDateTime($settings['deleted'], $conf->get('cfg_timezone'));
            $settings['deleted'] = $deleted->getTimestamp();
        }

        // We need to force summative management to be off (so that start dates will be set as defined).
        $management = $conf->get_setting('core', 'cfg_summative_mgmt');
        $conf->set_setting('cfg_summative_mgmt', false, $conf->get_setting_type('core', 'cfg_summative_mgmt'));

        $paper = new assessment($this->db, $conf);
        if (!empty($settings['calendaryear'])) {
            $settings['session'] = $settings['calendaryear'];
        } else {
            $yearutils = new yearutils($this->db);
            $supported = $yearutils->get_supported_years();

            $settings['session'] = $yearutils->get_current_session();

            if (!array_key_exists($settings['session'], $supported)) {
                $generator = new academic_year();
                $parameters['calendar_year'] = $settings['session'];
                $parameters['academic_year'] = $settings['session'] . '/' . (date('y') + 1);
                $generator->create_academic_year($parameters);
            }
        }

        $settings['moduleids'] = array();
        if (is_array($modulename)) {
            foreach ($modulename as $m) {
                $settings['moduleids'][] = self::test_get_moduleidbyname($m, $this->db);
            }
        } else {
            $settings['moduleids'][] = self::test_get_moduleidbyname($modulename, $this->db);
        }

        $settings['papertitle'] = $papertitle;
        $settings['papertype'] = $papertype;
        $settings['paperowner'] = $paperowner;

        try {
            $pid = $paper->create(
                $settings['papertitle'],
                $settings['papertype'],
                $settings['paperowner'],
                $settings['startdate'],
                $settings['enddate'],
                $settings['labs'],
                $settings['duration'],
                $settings['session'],
                $settings['moduleids'],
                $settings['timezone'],
                $settings['externalid'],
                $settings['externalsys'],
                $settings['remote'],
            );

            // Set the management type back to the correct state.
            $conf->set_setting('cfg_summative_mgmt', $management, $conf->get_setting_type('core', 'cfg_summative_mgmt'));
        } catch (Exception $e) {
            // Set the management type back to the correct state.
            $conf->set_setting('cfg_summative_mgmt', $management, $conf->get_setting_type('core', 'cfg_summative_mgmt'));

            $message = $e->getMessage();
            echo $message;
            throw new data_error('Error: ' . $message);
        }
        $settings['id'] = $pid;
        // Post creation settings may be provided as a json array.
        if (!empty($settings['settings'])) {
            $settings = array_merge($settings, $this->set_post_creation_settings($settings['id'], json_decode($settings['settings'], true)));
        } else {
            // Set defaults.
            $settings = array_merge($settings, $this->default_paper_properties);
        }

        // Set the paper to deleted if required.
        if (!is_null($settings['deleted'])) {
            $sql = $this->db->prepare('UPDATE properties SET deleted = ? WHERE property_id = ?');
            $sql->bind_param('si', $settings['deleted'], $pid);
            $sql->execute();
            $sql->close();
        }

        return $settings;
    }

    /**
     * Inserts security meta data restrictions for the paper.
     *
     * @param array $data
     * @return int The id of the record inserted.
     * @throws data_error
     */
    protected function insertSecurityMetadata(array $data): int
    {
        $sql = 'INSERT INTO paper_metadata_security (paperID, name, value) VALUES (?, ?, ?)';
        $query = $this->db->prepare($sql);
        $query->bind_param('iss', $data['paperID'], $data['name'], $data['value']);
        if (!$query->execute()) {
            // The metadata was not successfully inserted.
            throw new data_error("Security Metadata {$data['value']} for paper {$data['paperID']} was not inserted into database");
        }
        return $query->insert_id;
    }

    /**
     * Inserts (non security) meta data restrictions for the paper.
     *
     * @param array $data
     * @return int The id of the record inserted.
     * @throws data_error
     */
    protected function insertMetadata(array $data): int
    {
        $sql = 'INSERT INTO paper_metadata (paperID, name, value) VALUES (?, ?, ?)';
        $query = $this->db->prepare($sql);
        $query->bind_param('iss', $data['paperID'], $data['name'], $data['value']);
        if (!$query->execute()) {
            // The metadata was not successfully inserted.
            throw new data_error("Metadata {$data['value']} for paper {$data['paperID']} was not inserted into database");
        }
        return $query->insert_id;
    }

    /**
     * Set the paper properties after creation
     * @param integer $pid property id
     * @param array $parameters
     * @throws data_error If passed parameter is invalid
     * @return array
     */
    public function set_post_creation_settings(int $pid, array $parameters): array
    {
        $settings = $this->set_defaults_and_clean($this->default_paper_properties, $parameters);

        if (!is_null($settings['password'])) {
            $properties = \PaperProperties::get_paper_properties_by_id($pid, $this->db, '');
            $settings['password'] = $properties->encrypt_password($pid . $settings['password']);
        }
        if (!is_null($settings['external_review_deadline'])) {
            $external = new \DateTime($settings['external_review_deadline']);
            $external->setTime(12, 0);
            $settings['external_review_deadline'] = $external->format('Y-m-d H:i:s');
        }
        if (!is_null($settings['internal_review_deadline'])) {
            $internal = new \DateTime($settings['internal_review_deadline']);
            $internal->setTime(12, 0);
            $settings['internal_review_deadline'] = $internal->format('Y-m-d H:i:s');
        }

        foreach ($settings as $setting => $value) {
            if (!is_null($setting)) {
                $sql = $this->db->prepare('UPDATE properties SET ' . $setting . ' = ? WHERE property_id = ?');
                $sql->bind_param('si', $value, $pid);
                $sql->execute();
                $sql->close();
            }
        }
        return $settings;
    }

    /**
     * Get module id by name
     *
     * @param string $modulename
     * @param obj $db
     * @return int moduleid
     */
    public static function test_get_moduleidbyname($modulename, $db)
    {
        $result = $db->prepare('SELECT id FROM modules where fullname = ?');
        $result->bind_param('s', $modulename);
        $result->execute();
        $result->bind_result($moduleid);
        $result->store_result();
        $result->fetch();
        if ($result->num_rows == 0) {
            $result->close();
            return false;
        }
        $result->close();
        return $moduleid;
    }

    /**
     * Add reviwers to a paper
     *
     * @param array $parameters
     * Mandatories param in $parameters are
     *  string reviwer
     *  string paper
     *  string type - external|internal
     * @throws data_error If passed parameter is invalid
     * @return array
     */
    public function addReviewer($parameters): array
    {
        if (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        // Check that the right type has been passed.
        if (!is_array($parameters)) {
            throw new data_error('Must pass an array or object');
        }
        $types = array('external', 'internal');
        if (
            empty($parameters['reviewer'])
            or empty($parameters['paper'])
            or (!in_array($parameters['type'], $types))
        ) {
            throw new data_error('Error in | reviewer | paper | type |');
        } else {
            $parameters['paper'] = \Paper_utils::getPaperId($parameters['paper']);
            $parameters['reviewer'] = \UserUtils::username_exists($parameters['reviewer'], $this->db);
        }

        $insert = $this->db->prepare('INSERT INTO properties_reviewers VALUES(NULL, ?, ?, ?)');
        $insert->bind_param('iis', $parameters['paper'], $parameters['reviewer'], $parameters['type']);
        $insert->execute();
        $insert->close();

        return $parameters;
    }

    /**
     * Schedule a summative exam.
     *
     * @param array $parameters
     * Mandatories param in $parameters are
     *  string parameters[papertitle]
     *  string parameters[paperowner]
     *  string parameters[modulename]
     * @throws data_error If passed parameter is invalid
     * @return array
     */
    public function schedule($parameters): array
    {
        if (is_object($parameters)) {
            $parameters = (array)$parameters;
        }
        // Check that the right type has been passed.
        if (!is_array($parameters)) {
            throw new data_error('Must pass an array or object');
        }
        if (empty($parameters['papertitle']) or empty($parameters['paperowner']) or empty($parameters['modulename'])) {
            throw new data_error('Error in | papertitle | paperowner | modulename |');
        }

        $yearutils = new \yearutils($this->db);

        $default = array(
            'barriers' => 0,
            'cohortsize' => '<whole cohort>',
            'notes' => 'Some notes about the exam',
            'sittings' => 1,
            'campus' => 'Main Campus',
            'duration' => \date_utils::HOURSECS,
            'month' => rand(1, 12),
            'session' => $yearutils->get_current_session(),
            'remote' => 0,
        );
        $settings = $this->set_defaults_and_clean($default, $parameters);
        $config = Config::get_instance();
        $assessment = new \assessment($this->db, $config);

        $settings['papertitle'] = $parameters['papertitle'];
        $settings['papertype'] = \assessment::TYPE_SUMMATIVE;
        $settings['paperowner'] = \UserUtils::username_exists($parameters['paperowner'], $this->db);
        $modulename = $parameters['modulename'];

        // Ensure academic session exists.
        $supported = $yearutils->get_supported_years();
        if (!array_key_exists($settings['session'], $supported)) {
            $generator = new \academic_year();
            $parameters['calendar_year'] = $settings['session'];
            $parameters['academic_year'] = $settings['session'] . '/' . (date('y') + 1);
            $generator->create_academic_year($parameters);
        }

        // Get modules ids.
        $settings['moduleids'] = array();
        if (is_array($modulename)) {
            foreach ($modulename as $m) {
                $settings['moduleids'][] = self::test_get_moduleidbyname($m, $this->db);
            }
        } else {
            $settings['moduleids'][] = self::test_get_moduleidbyname($modulename, $this->db);
        }

        try {
            // Create template paper.
            $pid = $assessment->create(
                $settings['papertitle'],
                $settings['papertype'],
                $settings['paperowner'],
                null,
                null,
                '',
                $settings['duration'],
                $settings['session'],
                $settings['moduleids'],
                $config->get('cfg_timezone'),
                null,
                null,
                $settings['remote'],
            );

        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            throw new data_error('Error: ' . $message);
        }

        $settings['id'] = $pid;

        try {
            // Create schedule entry.
            $assessment->schedule(
                $settings['id'],
                $settings['month'],
                $settings['barriers'],
                $settings['cohortsize'],
                $settings['notes'],
                $settings['sittings'],
                $settings['campus']
            );
        } catch (Exception $e) {
            $message = $e->getMessage();
            echo $message;
            throw new data_error('Error: ' . $message);
        }
        return $settings;
    }
}
