<?php

// This file is part of Rogō
//
// Rogō is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogō is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogō.  If not, see <http://www.gnu.org/licenses/>.

$string['system'] = 'System';
$string['Add-Team-Member'] = 'Add Team Member';
$string['Remove-Team-Member'] = 'Remove Team Member';
$string['Remove-Enrolment'] = 'Module Un-Enrolment';
$string['Add-Enrolment'] = 'Module Enrolment';
$string['Add-Role'] = 'Add User Role';
$string['Remove-Role'] = 'Remove User Role';
$string['role'] = 'Role';
$string['module'] = 'Module';
$string['userdetails'] = 'User Details';
$string['selfenrol'] = 'Self Enrolment';
$string['lti'] = 'LTI';
$string['enrolapi'] = 'Enrolment API';
$string['userapi'] = 'User API';
$string['sms'] = 'SMS';
$string['systeminstall'] = 'System Install';
