// This file is part of Rogo
//
// Rogo is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Rogo is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Rogo.  If not, see <http://www.gnu.org/licenses/>.
//
// Form functions.
//
// @author Dr Joseph Baxter <joseph.baxter@nottingham.ac.uk>
// @copyright Copyright (c) 2019 The University of Nottingham
//
define(['jquery', 'jqueryvalidate'], function($) {
    return function () {
        /**
         * Initialise form validation.
         */
        this.init = function () {
            $('#theform').validate({
                errorClass: 'errfield',
                errorPlacement: function () {
                    return true;
                }
            });
            $('form').removeAttr('novalidate');
        };

        /**
         * Toggle item selection.
         * @param string objectID item id
         */
        this.toggle = function(objectID) {
            if ($('#div' + objectID).hasClass('r2')) {
                $('#div' + objectID).addClass('r1');
                $('#div' + objectID).removeClass('r2');
            } else {
                $('#div' + objectID).addClass('r2');
                $('#div' + objectID).removeClass('r1');
            }
        };
    }
});